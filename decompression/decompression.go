package decompression

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/h2non/filetype"

	"github.com/klauspost/compress/zstd"
	gzip "github.com/klauspost/pgzip"
	log "github.com/sirupsen/logrus"
	"github.com/ulikunitz/xz"
)

var (
	extToDecompressor = map[string]Decompressor{
		// supported by h2non/filetype
		"gz":  DecompressGZIP,
		"xz":  DecompressXZ,
		"zst": DecompressZSTD,
		// not supported by h2non/filetype, only by reading extension
		"raw": DecompressRAW,
		"lz4": DecompressLZ4,
	}

	extToDecompressorSparse = map[string]DecompressorSparse{
		"zst": DecompressZSTDSparse,
	}
)

type Decompressor func(io.Writer, io.Reader) error
type DecompressorSparse func(string, io.Reader) error

func MagicDecompressorBySeekable(dst io.Writer, src io.ReadSeeker) error {

	// check the size and error out if it is empty

	read, err := src.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}

	if read == 0 {
		return fmt.Errorf("src is empty!")
	}

	_, err = src.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	// get header

	header, err := ioutil.ReadAll(io.LimitReader(src, 262))
	if err != nil {
		return err
	}

	// match the header to the type

	src.Seek(0, io.SeekStart)

	kind, _ := filetype.Match(header)
	ext := strings.ToLower(kind.Extension)

	if decompr, ok := extToDecompressor[ext]; ok {
		return decompr(dst, src)
	}

	return fmt.Errorf("Couldn't figure out compression type.")

}

func MagicDecompressorByExtension(dst io.Writer, src io.Reader, filename string) error {

	ext := strings.ToLower(filepath.Ext(filename)[1:])

	if decompr, ok := extToDecompressor[ext]; ok {
		log.Debugf("Magic Decompressor: type is ")
		return decompr(dst, src)
	}

	return fmt.Errorf("Couldn't figure out compression typeof %s.", filename)

}

func MagicDecompressorSparseBySeekable(dst string, src io.ReadSeeker) error {

	// check the size and error out if it is empty

	read, err := src.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}

	if read == 0 {
		return fmt.Errorf("src is empty!")
	}

	_, err = src.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	// get header

	header, err := ioutil.ReadAll(io.LimitReader(src, 262))
	if err != nil {
		return err
	}

	_, err = src.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	// match the header to the type

	kind, _ := filetype.Match(header)
	ext := strings.ToLower(kind.Extension)

	if decompr, ok := extToDecompressorSparse[ext]; ok {
		return decompr(dst, src)
	}

	var decompr Decompressor
	decompr, ok := extToDecompressor[ext]

	if ok {
		log.Warnf("Type is %s, but sparse support is not implemented. Using non-sparse version.", kind.MIME.Type)
	} else {
		log.Warnf("Couldn't figure out compression type. Defaulting to RAW.")
		decompr = DecompressRAW
	}

	file, err := os.Create(dst)

	if err != nil {
		return fmt.Errorf("failed to create '%s': %v", dst, err)
	}
	defer file.Close()

	return decompr(file, src)

}

func DecompressGZIP(dst io.Writer, src io.Reader) error {

	log.Infof("using gzip")
	gzr, err := gzip.NewReader(src)
	if err != nil {
		return fmt.Errorf("gzip reader: %v", err)
	}
	defer gzr.Close()

	written, err := io.Copy(dst, gzr)
	if err != nil {
		return fmt.Errorf("copy: %v", err)
	}

	if written == 0 {
		return fmt.Errorf("wrote 0 bytes!")
	}

	return nil

}

func DecompressXZ(dst io.Writer, src io.Reader) error {

	log.Infof("using xz")
	xzr, err := xz.NewReader(src)
	if err != nil {
		return fmt.Errorf("xz reader: %v", err)
	}

	written, err := io.Copy(dst, xzr)
	if err != nil {
		return fmt.Errorf("copy: %v", err)
	}

	if written == 0 {
		return fmt.Errorf("wrote 0 bytes!")
	}

	return nil

}

func DecompressRAW(dst io.Writer, src io.Reader) error {

	log.Infof("using raw")

	written, err := io.Copy(dst, src)
	if err != nil {
		return fmt.Errorf("copy: %v", err)
	}

	if written == 0 {
		return fmt.Errorf("wrote 0 bytes!")
	}

	return nil

}

func checkZSTDBinary() error {
	// "zstd check ok!\n" compresesed into zst
	data := []byte{
		0x28, 0xB5, 0x2F, 0xFD,
		0x24, 0x0F, 0x79, 0x00,
		0x00, 0x7A, 0x73, 0x74,
		0x64, 0x20, 0x63, 0x68,
		0x65, 0x63, 0x6B, 0x20,
		0x6F, 0x6B, 0x21, 0x0A,
		0xD5, 0xE6, 0xDA, 0x8B,
	}

	cmd := exec.Command("zstd", "-d", "-T0", "-f", "-c", "--no-sparse")
	cmd.Stdin = bytes.NewReader(data)
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()

	if err == nil && stderr.String() == "" && stdout.String() == "zstd check ok!\n" {
		return nil
	}

	return fmt.Errorf("could not find zstd binary:\n  stdout: %s\n  stderr: %s\n  err: %v", stdout, stderr, err)
}

func DecompressZSTD(dst io.Writer, src io.Reader) error {
	// for some reason, the zstd binary is much faster than
	// the pure golang version
	// so, use it if we have it

	if err := checkZSTDBinary(); err == nil {
		return DecompressZSTDBinary(dst, src)
	} else {
		log.Warnf("Defaulting to zstd golang implementation: %+v", err)
		return DecompressZSTDGolang(dst, src)
	}

}

func DecompressZSTDBinary(dst io.Writer, src io.Reader) error {

	log.Infof("using zstd binary")

	// -d: Decompress
	// -T0: Run with maximum threads
	// -c: write to stdout
	// --no-sparse: do not write sparsely
	cmd := exec.Command("zstd", "-d", "-T0", "-c", "--no-sparse")
	cmd.Stdin = src
	cmd.Stdout = dst
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()
	if err != nil || stderr.String() != "" {
		return fmt.Errorf("exec zstd: \"%s\" %v", stderr.String(), err)
	}

	return nil
}

func DecompressZSTDGolang(dst io.Writer, src io.Reader) error {

	log.Infof("using zstd golang")

	zstdr, err := zstd.NewReader(src)
	if err != nil {
		return fmt.Errorf("zstd reader: %v", err)
	}
	defer zstdr.Close()

	_, err = io.Copy(dst, zstdr)
	if err != nil {
		return fmt.Errorf("copy: %v", err)
	}

	return nil

}

func DecompressZSTDSparse(dst string, src io.Reader) error {

	if err := checkZSTDBinary(); err != nil {
		return err
	}

	log.Infof("using zstd binary")

	// -d: Decompress
	// -T0: Run with maximum threads
	// -f: Skip input/output checks
	// --sparse: Write the image sparsely
	// -o: Output file
	cmd := exec.Command("zstd", "-d", "-T0", "-f", "--sparse", "-o", dst)
	cmd.Stdin = src
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("exec zstd: \"%s\", \"%s\" %v", stdout.String(), stderr.String(), err)
	}

	return nil
}

func checkLZ4Binary() error {
	// "lz4 check ok!\n" compresesed into zst
	data := []byte{
		0x04, 0x22, 0x4D, 0x18,
		0x64, 0x40, 0xA7, 0x0E,
		0x00, 0x00, 0x80, 0x6C,
		0x7A, 0x34, 0x20, 0x63,
		0x68, 0x65, 0x63, 0x6B,
		0x20, 0x6F, 0x6B, 0x21,
		0x0A, 0x00, 0x00, 0x00,
		0x00, 0x5B, 0x2F, 0xFD,
		0xA0,
	}

	cmd := exec.Command("lz4", "-d", "-f", "-c", "--no-sparse")
	cmd.Stdin = bytes.NewReader(data)
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()

	if err == nil && stderr.String() == "" && stdout.String() == "lz4 check ok!\n" {
		return nil
	}

	return fmt.Errorf("could not find lz4 binary:\n  stdout: %s\n  stderr: %s\n  err: %v", stdout, stderr, err)
}

// As of May 2022, the Golang version of LZ4 breaks with Minio, so we are not including it.
func DecompressLZ4(dst io.Writer, src io.Reader) error {

	if err := checkLZ4Binary(); err != nil {
		return err
	}

	log.Infof("using lz4 binary")

	// -d: Decompress
	// -f: Skip input/output checks
	// -c: write to stdout
	// --no-sparse: do not write sparsely
	cmd := exec.Command("lz4", "-d", "-f", "-c", "--no-sparse")
	cmd.Stdin = src
	cmd.Stdout = dst
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("exec lz4: \"%s\" %v", stderr.String(), err)
	}

	return nil
}

func DecompressLZ4Sparse(dst string, src io.Reader) error {

	if err := checkLZ4Binary(); err != nil {
		return err
	}

	log.Infof("using lz4 binary")

	// -d: Decompress
	// -f: Skip input/output checks
	// --sparse: Write the image sparsely
	// -o: Output file
	cmd := exec.Command("lz4", "-d", "-f", "--sparse", "-o", dst)
	cmd.Stdin = src
	stdout := new(bytes.Buffer)
	cmd.Stdout = stdout
	stderr := new(bytes.Buffer)
	cmd.Stderr = stderr

	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("exec lz4: \"%s\" %v", stderr.String(), err)
	}

	return nil
}
