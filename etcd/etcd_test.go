package etcd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTxnPutGet(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	assert := assert.New(t)

	var ops EtcdTransaction

	k1 := NewGenericEtcdKey("hello", []byte("world"))
	k2 := NewGenericEtcdKey("hello2", []byte("world"))

	err := k1.AppendCreateOps(etcdCli, &ops)
	assert.Nil(err)
	err = k2.AppendCreateOps(etcdCli, &ops)
	assert.Nil(err)

	txr, _, err := ops.EtcdTx(etcdCli, "")

	m, err := (*GenericEtcdKey).ReadAllFromResponse(nil, txr)
	assert.Nil(err)
	assert.Equal(
		0,
		len(m),
		"should have zero elements",
	)

	ops = EtcdTransaction{}
	err = k1.AppendReadOps(etcdCli, &ops)
	assert.Nil(err)

	err = k2.AppendReadOps(etcdCli, &ops)
	assert.Nil(err)

	txr, _, err = ops.EtcdTx(etcdCli, "")
	m, err = (*GenericEtcdKey).ReadAllFromResponse(nil, txr)
	assert.Nil(err)
	assert.Equal(
		2,
		len(m),
		"should have two elements",
	)
	logger.Sugar().Infof("%s", m)
	assert.NotNil(m[k1.Key()], k1.Key())
	assert.NotNil(m[k2.Key()], k2.Key())
}

func TestTxnGetCache(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	assert := assert.New(t)

	var ops EtcdTransaction

	cache := NewReadCache(0, nil)

	k1 := NewGenericEtcdKey("hello", []byte("world"))
	k2 := NewGenericEtcdKey("hello2", []byte("world"))

	cache.Set(*k1)
	cache.Set(*k2)

	ops = EtcdTransaction{}
	ops.Read(k1, k2)

	txr, _, err := ops.EtcdTxWithOptions(etcdCli, EtcdTransactionOptions{
		Cache: cache,
	})
	m, err := (*GenericEtcdKey).ReadAllFromResponse(nil, txr)
	assert.Nil(err)
	assert.Equal(
		2,
		len(m),
		"should have two elements",
	)
	logger.Sugar().Infof("%s", m)
	assert.NotNil(m[k1.Key()], k1.Key())
	assert.NotNil(m[k2.Key()], k2.Key())
}
