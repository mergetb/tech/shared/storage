package etcd

import (
	"context"
	"fmt"
	"path"
	"strings"
	"time"

	pb "go.etcd.io/etcd/api/v3/etcdserverpb"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
)

const (
	contextTimeout = 30 * time.Second
)

// Joins the prefix and each element with the prefix removed from each element
func SmartJoin(prefix string, a ...string) string {

	toJoin := make([]string, 0, len(a)+1)
	toJoin = append(toJoin, prefix)

	for _, s := range a {
		toJoin = append(toJoin, strings.TrimPrefix(s, prefix))
	}

	return path.Join(toJoin...)
}

func GetCurrentRevision(cli *clientv3.Client) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	// this is a transaction that does nothing just so we can get the current revision
	resp, err := cli.Txn(ctx).Commit()
	if err != nil {
		return -1, err
	}

	revision := resp.Header.GetRevision()
	if revision <= 0 {
		return -1, fmt.Errorf("revision did not exist!")
	}

	return revision, nil
}

func keysToTxrResponses(ges ...GenericEtcdKey) *pb.ResponseOp {
	var kvs []*mvccpb.KeyValue

	for _, ge := range ges {
		kvs = append(kvs, ge.toMVCCPBKV())
	}

	return &pb.ResponseOp{
		Response: &pb.ResponseOp_ResponseRange{
			ResponseRange: &pb.RangeResponse{
				More:  false,
				Count: int64(len(ges)),
				Kvs:   kvs,
			},
		},
	}
}

func JoinTxr(t1, t2 *clientv3.TxnResponse) *clientv3.TxnResponse {
	if t1 == nil {
		return t2
	}

	if t2 == nil {
		return t1
	}

	ans := new(clientv3.TxnResponse)

	// use the original header, as it has the lower revision number
	ans.Header = t1.Header
	ans.Succeeded = t1.Succeeded && t2.Succeeded

	for _, r := range t1.Responses {
		ans.Responses = append(ans.Responses, r)
	}

	for _, r := range t2.Responses {
		ans.Responses = append(ans.Responses, r)
	}

	return ans
}
