package etcd

import (
	"sync"
	"time"

	"github.com/tidwall/btree"
	"go.etcd.io/etcd/client/v3"
)

type cacheItem struct {
	GenericEtcdKey
	expires time.Time
}

func newCacheItem(key string) cacheItem {
	return cacheItem{
		GenericEtcdKey: GenericEtcdKey{
			EtcdKey: key,
		},
	}
}

func (c cacheItem) isExpired(now time.Time) bool {
	return c.expires != time.Time{} && now.After(c.expires)
}

func (c cacheItem) Less(o cacheItem) bool {
	return c.EtcdKey < o.EtcdKey
}

type EtcdCacheDurationGetter func(item GenericEtcdKey) time.Duration

type EtcdCache struct {
	items  *btree.BTreeG[cacheItem]
	getter EtcdCacheDurationGetter
	// BTreeG has a PathHint object that you can pass around:
	// https://github.com/tidwall/btree/blob/master/PATH_HINT.md
	// If you pass it around, you can get faster performance,
	// so might as well pass it around
	// The package exposes it so you decide on the "path hint policy,"
	// which is generally, 1 path hint per thread/client
	hint     btree.PathHint
	close    chan struct{}
	revision int64
	revlock  sync.RWMutex
}

// 0xff doesn't have a next prefix, so we should get whatever noPrefixEnd is
var noPrefixEnd = clientv3.GetPrefixRangeEnd(string([]byte{0xff}))

func NewReadCache(cleaningInterval time.Duration, getter EtcdCacheDurationGetter) *EtcdCache {
	bt := btree.NewBTreeG[cacheItem]((cacheItem).Less)

	if getter == nil {
		getter = func(item GenericEtcdKey) time.Duration {
			return cleaningInterval
		}
	}

	rc := &EtcdCache{
		items:  bt,
		close:  make(chan struct{}),
		getter: getter,
	}

	// if the interval is 0, don't clean, just wait for the exit
	if cleaningInterval == 0 {
		go func() {
			<-rc.close
		}()

		return rc
	}

	// periodic cleaner
	go func() {
		ticker := time.NewTicker(cleaningInterval)
		defer ticker.Stop()

		for {
			select {
			case <-ticker.C:
				func() {

					now := time.Now()

					for _, item := range rc.items.Items() {
						if item.isExpired(now) {
							rc.items.DeleteHint(item, &rc.hint)
						}
					}
				}()

			case <-rc.close:
				return
			}
		}
	}()

	return rc
}

func (rc *EtcdCache) GetRevision() int64 {
	rc.revlock.RLock()
	defer rc.revlock.RUnlock()

	return rc.revision
}

func (rc *EtcdCache) bumpRevision(rev int64) bool {
	rc.revlock.Lock()
	defer rc.revlock.Unlock()

	if rev > rc.revision {
		rc.revision = rev
		return true
	}

	return false
}

func (rc *EtcdCache) Get(key string, opts ...clientv3.OpOption) (ge GenericEtcdKey, found bool) {

	c := newCacheItem(key)

	item, found := rc.items.GetHint(c, &rc.hint)

	if !found || item.isExpired(time.Now()) {
		found = false
		return
	}

	op := clientv3.OpGet(key, opts...)
	ge = item.GenericEtcdKey
	if !(&ge).fulfillsEtcdOps(op) {
		ge = GenericEtcdKey{}
		found = false
		return
	}

	return
}

func (rc *EtcdCache) GetAll(opts ...clientv3.OpOption) (keys []GenericEtcdKey) {
	return rc.GetRange("", noPrefixEnd, opts...)
}

func (rc *EtcdCache) GetPrefix(prefix string, opts ...clientv3.OpOption) (keys []GenericEtcdKey) {
	return rc.GetRange(prefix, clientv3.GetPrefixRangeEnd(prefix), opts...)
}

func (rc *EtcdCache) GetRange(start, end string, opts ...clientv3.OpOption) (keys []GenericEtcdKey) {
	op := clientv3.OpGet(start, opts...)

	now := time.Now()

	a := newCacheItem(start)
	b := newCacheItem(end)

	iterator := func(item cacheItem) bool {
		if end != noPrefixEnd && !item.Less(b) {
			return false
		}

		if !item.isExpired(now) {
			ge := item.GenericEtcdKey
			if (&ge).fulfillsEtcdOps(op) {
				keys = append(keys, ge)
			}

		}

		return true
	}

	rc.items.AscendHint(a, iterator, &rc.hint)

	return
}

func (rc *EtcdCache) Delete(key string) (prev GenericEtcdKey, found bool) {

	c := newCacheItem(key)

	item, found := rc.items.DeleteHint(c, &rc.hint)
	// if not found or expired
	if !found || item.isExpired(time.Now()) {
		found = false
		return
	}

	prev = item.GenericEtcdKey
	return
}

func (rc *EtcdCache) DeletePrefix(prefix string) (prev []GenericEtcdKey) {
	return rc.DeleteRange(prefix, clientv3.GetPrefixRangeEnd(prefix))
}

func (rc *EtcdCache) DeleteRange(start, end string) (prev []GenericEtcdKey) {

	now := time.Now()

	a := newCacheItem(start)
	b := newCacheItem(end)

	var toDelete []cacheItem

	iterator := func(item cacheItem) bool {
		if end != noPrefixEnd && !item.Less(b) {
			return false
		}

		toDelete = append(toDelete, item)

		return true
	}

	rc.items.AscendHint(a, iterator, &rc.hint)

	for _, item := range toDelete {
		rc.items.DeleteHint(item, &rc.hint)

		if !item.isExpired(now) {
			prev = append(prev, item.GenericEtcdKey)
		}
	}

	return
}

func (rc *EtcdCache) Set(ge GenericEtcdKey) (prev GenericEtcdKey, found bool) {
	var expires time.Time

	duration := rc.getter(ge)

	if duration > 0 {
		expires = time.Now().Add(duration)
	}

	c := cacheItem{
		GenericEtcdKey: ge,
		expires:        expires,
	}

	item, found := rc.items.SetHint(c, &rc.hint)
	// if not found or expired
	if !found || item.isExpired(time.Now()) {
		found = false
		return
	}

	prev = item.GenericEtcdKey
	return
}

// This returns a new EtcdCache with the same underlying pointers,
// but with a new hint item.
// Use this when you want multiple threads/clients working on the same EtcdCache,
// but a different hinter per thread/clients.
func (rc *EtcdCache) NewHint() *EtcdCache {
	return &EtcdCache{
		items:  rc.items,
		getter: rc.getter,
		hint:   rc.hint,
		close:  rc.close,
	}
}

func (rc *EtcdCache) Close() {
	rc.close <- struct{}{}
	rc.items.Clear()
}
