package etcd

import (
	"bytes"

	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
)

type GenericEtcdKey struct {
	EtcdKey        string
	EtcdValue      []byte
	Version        int64
	ModRevision    int64
	CreateRevision int64
	Lease          int64
}

func NewGenericEtcdKey(key string, value []byte) *GenericEtcdKey {
	return &GenericEtcdKey{
		EtcdKey:        key,
		EtcdValue:      value,
		Version:        -1,
		ModRevision:    -1,
		CreateRevision: -1,
		Lease:          0,
	}
}

func NewGenericEtcdKeyFromKV(kv *mvccpb.KeyValue) *GenericEtcdKey {
	return &GenericEtcdKey{
		EtcdKey:        string(kv.Key),
		EtcdValue:      kv.Value,
		Version:        kv.Version,
		ModRevision:    kv.ModRevision,
		CreateRevision: kv.CreateRevision,
		Lease:          kv.Lease,
	}
}

func NewGenericEtcdKeyFromEtcdObjectIO(o EtcdObjectIO) (*GenericEtcdKey, error) {
	value, err := MarshalEtcdObjectIO(o)
	if err != nil {
		return nil, err
	}

	return &GenericEtcdKey{
		EtcdKey:   o.Key(),
		EtcdValue: value,
		Version:   o.GetVersion(),
	}, nil
}

func (g *GenericEtcdKey) toMVCCPBKV() *mvccpb.KeyValue {
	return &mvccpb.KeyValue{
		Key:            []byte(g.EtcdKey),
		CreateRevision: g.CreateRevision,
		ModRevision:    g.ModRevision,
		Version:        g.Version,
		Value:          g.EtcdValue,
		Lease:          g.Lease,
	}
}

func (g *GenericEtcdKey) Equals(o *GenericEtcdKey) bool {
	if g == nil && o == nil {
		return true
	}

	// at least 1 is not nil, so if 1 is nil, the other is not
	if g == nil || o == nil {
		return false
	}

	return g.EtcdKey == o.EtcdKey &&
		// ignore version numbers if either of them are less than 0
		(g.Version < 0 || o.Version < 0 || g.Version == o.Version) &&
		bytes.Equal(g.EtcdValue, o.EtcdValue)
}

// generic etcd key: StorageMessage Implementation
func (g *GenericEtcdKey) Marshal() ([]byte, error) {
	return g.EtcdValue, nil
}

func (g *GenericEtcdKey) Unmarshal(b []byte) error {
	g.EtcdValue = b
	return nil
}

// generic etcd key: EtcdObjectIO Implementation

func (g *GenericEtcdKey) Key() string {
	return g.EtcdKey
}

func (g *GenericEtcdKey) Value() interface{} {
	return g.EtcdValue
}

func (g *GenericEtcdKey) GetVersion() int64 {
	return g.Version
}

func (g *GenericEtcdKey) SetVersion(v int64) {
	g.Version = v
}

// generic etcd key: EtcdBufferedObject implementation

func (g *GenericEtcdKey) AppendReadOps(etcdCli *clientv3.Client, ops *EtcdTransaction) error {
	ops.Read(g)

	return nil
}

func (g *GenericEtcdKey) AppendCreateOps(etcdCli *clientv3.Client, ops *EtcdTransaction) error {
	ops.Write(g)

	return nil
}

func (g *GenericEtcdKey) AppendUpdateOps(etcdCli *clientv3.Client, ops *EtcdTransaction) error {
	ops.Write(g)

	return nil
}

func (g *GenericEtcdKey) AppendDeleteOps(etcdCli *clientv3.Client, ops *EtcdTransaction) error {
	ops.Delete(g)

	return nil
}

func (_ *GenericEtcdKey) ReadAllFromResponse(txn *clientv3.TxnResponse) (map[string]EtcdObjectIO, error) {

	m := make(map[string]EtcdObjectIO)

	for _, r := range txn.Responses {
		rr := r.GetResponseRange()

		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {
			m[string(kv.Key)] = NewGenericEtcdKeyFromKV(kv)
		}

	}

	return m, nil

}

func (g *GenericEtcdKey) Read(etcdCli *clientv3.Client) error {
	return UnbufferedRead(etcdCli, g)
}

func (g *GenericEtcdKey) Create(etcdCli *clientv3.Client) (*EtcdRollback, error) {
	return UnbufferedCreate(etcdCli, g)
}

func (g *GenericEtcdKey) Update(etcdCli *clientv3.Client) (*EtcdRollback, error) {
	return UnbufferedUpdate(etcdCli, g)
}

func (g *GenericEtcdKey) Delete(etcdCli *clientv3.Client) (*EtcdRollback, error) {
	return UnbufferedDelete(etcdCli, g)
}

func (g *GenericEtcdKey) fulfillsEtcdOps(op clientv3.Op) bool {
	if g == nil {
		return false
	}

	ok := ((op.Rev() == 0) && // unsupported if non zero
		(!op.IsCountOnly()) && // unsupported if set
		(op.MaxCreateRev() == 0 || op.MaxCreateRev() >= g.CreateRevision) &&
		(op.MaxModRev() == 0 || op.MaxModRev() >= g.ModRevision) &&
		(op.MinCreateRev() == 0 || op.MinCreateRev() <= g.CreateRevision) &&
		(op.MinModRev() == 0 || op.MinModRev() <= g.ModRevision))

	if ok && op.IsKeysOnly() {
		g.EtcdValue = nil
	}

	return ok
}
