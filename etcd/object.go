package etcd

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	"go.etcd.io/etcd/client/v3"
)

// Support for types that want to marshal and unmarshal themselves.
type StorageMessage interface {
	Marshal() ([]byte, error)
	Unmarshal([]byte) error
}

// Support for types that want to have EtcdTx write a timestamp
// immediately before the transaction is committed.
type EtcdTxTimestamped interface {
	WriteTimestamp()
}

type EtcdObjectIO interface {
	Key() string
	Value() interface{}
	GetVersion() int64
	SetVersion(int64)
}

type EtcdObject interface {
	EtcdObjectIO
	Create(*clientv3.Client) (*EtcdRollback, error)
	Read(*clientv3.Client) error
	Update(*clientv3.Client) (*EtcdRollback, error)
	Delete(*clientv3.Client) (*EtcdRollback, error)
}

type EtcdBufferedObject interface {
	EtcdObjectIO
	AppendReadOps(*clientv3.Client, *EtcdTransaction) error
	AppendCreateOps(*clientv3.Client, *EtcdTransaction) error
	AppendUpdateOps(*clientv3.Client, *EtcdTransaction) error
	AppendDeleteOps(*clientv3.Client, *EtcdTransaction) error
	// this function is a little bit different;
	// it is a static method that shouldn't use any of the object's state
	ReadAllFromResponse(*clientv3.TxnResponse) (map[string]EtcdObjectIO, error)
}

// These are helper functions to deal with the common case of not actually buffering the operation
// since we want to immediately execute it for one operation.
// See: generickey.go for an example of usage of these functions.

func UnbufferedCreate(etcdCli *clientv3.Client, g EtcdBufferedObject) (*EtcdRollback, error) {
	ops := new(EtcdTransaction)

	err := g.AppendCreateOps(etcdCli, ops)
	if err != nil {
		return nil, err
	}

	_, r, err := ops.EtcdTx(etcdCli, "")

	return r, err
}

func UnbufferedUpdate(etcdCli *clientv3.Client, g EtcdBufferedObject) (*EtcdRollback, error) {
	ops := new(EtcdTransaction)

	err := g.AppendUpdateOps(etcdCli, ops)
	if err != nil {
		return nil, err
	}

	_, r, err := ops.EtcdTx(etcdCli, "")

	return r, err
}

func UnbufferedDelete(etcdCli *clientv3.Client, g EtcdBufferedObject) (*EtcdRollback, error) {
	ops := new(EtcdTransaction)

	err := g.AppendDeleteOps(etcdCli, ops)
	if err != nil {
		return nil, err
	}

	_, r, err := ops.EtcdTx(etcdCli, "")

	return r, err
}

func UnbufferedRead(etcdCli *clientv3.Client, g EtcdBufferedObject) error {
	ops := new(EtcdTransaction)

	err := g.AppendReadOps(etcdCli, ops)
	if err != nil {
		return err
	}

	txr, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		return err
	}

	m, err := g.ReadAllFromResponse(txr)
	if err != nil {
		return err
	}

	if ans, ok := m[g.Key()]; ok {
		// We have to marshal and unmarshal the value since EtcdObjectIO.Value()
		// returns an interface{} and not a []byte

		data, err := MarshalEtcdObjectIO(ans)
		if err != nil {
			return err
		}

		err = UnmarshalEtcdObjectIO(g, data)
		if err != nil {
			return err
		}

		g.SetVersion(ans.GetVersion())

		return nil
	}

	return fmt.Errorf("%s not found!", g.Key())
}

func MarshalEtcdObjectIO(o EtcdObjectIO) ([]byte, error) {
	val := o.Value()

	switch val.(type) {
	case proto.Message:
		return proto.Marshal(val.(proto.Message))
	case StorageMessage:
		return val.(StorageMessage).Marshal()
	case []byte:
		return val.([]byte), nil
	default:
		return nil, fmt.Errorf("unsupported storage type: %T", o.Value())
	}
}

func UnmarshalEtcdObjectIO(o EtcdObjectIO, b []byte) error {
	switch o.(type) {
	case proto.Message:
		return proto.Unmarshal(b, o.(proto.Message))
	case StorageMessage:
		return o.(StorageMessage).Unmarshal(b)
	case *GenericEtcdKey:
		o := o.(*GenericEtcdKey)
		o.EtcdValue = b
	default:
		return fmt.Errorf("unsupported storage type: %T", o)
	}

	return nil
}
