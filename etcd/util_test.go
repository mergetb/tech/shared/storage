package etcd

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var etcdCli *clientv3.Client
var logger *zap.Logger

func createEtcdClient(debug bool) (*clientv3.Client, error) {

	endpoint := os.Getenv("TEST_ETCD_ENDPOINT") // may be set be CI or other scripts
	if endpoint == "" {
		endpoint = "localhost:2379"
	}
	log.Infof("endpoint: %s", endpoint)

	cfg := clientv3.Config{
		Endpoints:            []string{endpoint},
		DialKeepAliveTime:    6 * time.Minute,
		DialKeepAliveTimeout: 20 * time.Second,
		PermitWithoutStream:  true,
	}

	if debug {
		lg, err := zap.NewProduction()
		if err != nil {
			return nil, err
		}

		cfg.Logger = lg
		cfg.DialOptions = []grpc.DialOption{
			grpc.WithChainStreamInterceptor(logging.StreamClientInterceptor(InterceptorLogger(lg))),
			grpc.WithChainUnaryInterceptor(logging.UnaryClientInterceptor(InterceptorLogger(lg))),
			grpc.WithStreamInterceptor(logging.StreamClientInterceptor(InterceptorLogger(lg))),
		}
	}

	cli, err := clientv3.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("etcd client: %v", err)
	}

	return cli, nil
}

func InterceptorLogger(l *zap.Logger) logging.Logger {
	return logging.LoggerFunc(func(ctx context.Context, lvl logging.Level, msg string, fields ...any) {
		f := make([]zap.Field, 0, len(fields)/2)

		for i := 0; i < len(fields); i += 2 {
			key := fields[i]
			value := fields[i+1]

			switch v := value.(type) {
			case string:
				f = append(f, zap.String(key.(string), v))
			case int:
				f = append(f, zap.Int(key.(string), v))
			case bool:
				f = append(f, zap.Bool(key.(string), v))
			default:
				f = append(f, zap.Any(key.(string), v))
			}
		}

		logger := l.WithOptions(zap.AddCallerSkip(1)).With(f...)

		switch lvl {
		case logging.LevelDebug:
			logger.Debug(msg)
		case logging.LevelInfo:
			logger.Info(msg)
		case logging.LevelWarn:
			logger.Warn(msg)
		case logging.LevelError:
			logger.Error(msg)
		default:
			panic(fmt.Sprintf("unknown level %v", lvl))
		}
	})
}

func TestMain(m *testing.M) {
	// the tests require things to be done in an exact order,
	// and setting this to 100 will affect the order
	// for actual execution, the difference only shows up with async,
	// which is assumed to be already arbitrary order

	logger, _ = zap.NewDevelopment()

	cli, err := createEtcdClient(false)

	if err != nil {
		fmt.Printf("init error: %v", err)
		os.Exit(1)
	}

	etcdCli = cli

	os.Exit(m.Run())
}

func clearEtcd() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	etcdCli.Delete(ctx, "", clientv3.WithPrefix())
}
