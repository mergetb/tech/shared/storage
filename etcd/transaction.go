package etcd

import (
	"context"
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	pb "go.etcd.io/etcd/api/v3/etcdserverpb"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
)

type Operation int

const (
	Read Operation = iota + 1
	Write
	Delete
)

const DEFAULT_TXN_LIMIT = 128

type StorOp struct {
	Op     Operation
	Object EtcdObjectIO
	Opts   []clientv3.OpOption

	returnedResponse  *pb.ResponseOp
	responseFromCache bool
}

func (so *StorOp) String() string {
	op := ""
	switch so.Op {
	case Read:
		op = "Read"
	case Write:
		op = "Write"
	case Delete:
		op = "Delete"
	}

	return fmt.Sprintf("[%s %s]", op, so.Object.Key())
}

type EtcdTransaction struct {
	Ops []StorOp
	Ifs []clientv3.Cmp

	txnToOpsMap []int
	options     *EtcdTransactionOptions
}

type EtcdWatchResponse struct {
	clientv3.WatchResponse
	IndexOfWatch int
}

type EtcdWatchChannel <-chan EtcdWatchResponse

type EtcdTransactionOptions struct {
	Ctx context.Context
	// if ctx is nil, it will make one for you for transactions

	TxLimit int
	// < 0: will always do it it on transaction, 0: changes it to DEFAULT_TXN_LIMIT

	Lock string
	// lock provides a means for the package to create and release a lock based upon the key. empty string avoids locking

	IgnoreFailedIfs bool
	// means that if the txn wasn't successful (usually due to failure on if op conditions), to ignore or not

	StartRevision int64
	// the revision to do key reads or when to start etcd watches
	//   < 0: use the current revision of etcd
	//   = 0: don't do anything with it
	//   > 0: actually used
	// this is useful if your transaction has to be split across

	Cache *EtcdCache
	// when doing etcd reads, if the key is already in the cache, we read from the cache instead and skip that operation
	// the cache is updated automatically after a transaction/watch response
	// certain read options are unsupported, and if so, will just read from etcd
}

func (t *EtcdTransaction) String() string {
	if t == nil {
		return "nil"
	}

	s_ops := ""
	for _, op := range t.Ops {
		s_ops += op.String() + " "
	}

	return fmt.Sprintf("IF: %+v, Ops: %+v", t.Ifs, s_ops)
}

func (t *EtcdTransaction) ReadWithOpts(o EtcdObjectIO, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Read,
		Object: o,
		Opts:   opts,
	})

}

func (t *EtcdTransaction) Read(a ...EtcdObjectIO) {

	for _, o := range a {
		t.Ops = append(t.Ops, StorOp{
			Op:     Read,
			Object: o,
		})
	}

}

func (t *EtcdTransaction) ReadKey(keys ...string) {

	for _, k := range keys {
		t.Ops = append(t.Ops, StorOp{
			Op:     Read,
			Object: NewGenericEtcdKey(k, nil),
		})
	}

}

func (t *EtcdTransaction) ReadKeyWithOpt(k string, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Read,
		Object: NewGenericEtcdKey(k, nil),
		Opts:   opts,
	})

}

func (t *EtcdTransaction) WriteWithOpt(o EtcdObjectIO, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Write,
		Object: o,
		Opts:   opts,
	})
}

func (t *EtcdTransaction) Write(a ...EtcdObjectIO) {

	for _, o := range a {
		t.Ops = append(t.Ops, StorOp{
			Op:     Write,
			Object: o,
		})
	}
}

func (t *EtcdTransaction) WriteKey(key string, value []byte) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Write,
		Object: NewGenericEtcdKey(key, value),
	})
}

func (t *EtcdTransaction) WriteKeyWithOpt(key string, value []byte, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Write,
		Object: NewGenericEtcdKey(key, value),
		Opts:   opts,
	})
}

func (t *EtcdTransaction) DeleteWithOpt(o EtcdObjectIO, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Delete,
		Object: o,
		Opts:   opts,
	})

}

func (t *EtcdTransaction) Delete(a ...EtcdObjectIO) {

	for _, o := range a {
		t.Ops = append(t.Ops, StorOp{
			Op:     Delete,
			Object: o,
		})
	}

}

func (t *EtcdTransaction) DeleteKey(keys ...string) {

	for _, k := range keys {
		t.Ops = append(t.Ops, StorOp{
			Op:     Delete,
			Object: NewGenericEtcdKey(k, nil),
		})
	}

}

func (t *EtcdTransaction) DeleteKeyWithOpt(k string, opts ...clientv3.OpOption) {

	t.Ops = append(t.Ops, StorOp{
		Op:     Delete,
		Object: NewGenericEtcdKey(k, nil),
		Opts:   opts,
	})

}

func (t *EtcdTransaction) If(c clientv3.Cmp) {
	t.Ifs = append(t.Ifs, c)
}

// Appends all of the other's transactions into this one
func (t *EtcdTransaction) Merge(other *EtcdTransaction) {
	if other == nil {
		return
	}

	t.Ops = append(t.Ops, other.Ops...)
	t.Ifs = append(t.Ifs, other.Ifs...)
}

type RbStack []*EtcdRollback

func (rbs RbStack) Push(rb *EtcdRollback) RbStack {

	if rb == nil {
		// don't push things we cannot unwind
		return rbs
	}

	return append(rbs, rb)

}

func (rbs RbStack) Pop() (*EtcdRollback, RbStack) {

	n := len(rbs)
	return rbs[n-1], rbs[:n-1]

}

func (rbs RbStack) Unwind() error {
	for i := len(rbs) - 1; i >= 0; i-- {
		if rbs[i] != nil {
			resp, err := (*rbs[i])()
			if err != nil {
				return err
			}
			if !resp.Succeeded {
				return fmt.Errorf("transaction failed")
			}
		}
	}
	return nil
}

type EtcdRollback func() (*clientv3.TxnResponse, error)

func (rb *EtcdRollback) Rollback() (*clientv3.TxnResponse, error) {
	if rb == nil {
		return nil, nil
	}

	return (*rb)()

}

// This calls EtcdTxWithOptions with tx_limit: 0 and IgnoreFailedIfs: false
// This means by default, the txn will be segmented using DEFAULT_TXN_LIMIT ops per segment
// and that if the txn was unsuccessful (due to if condition failures), it will return an error
func (t *EtcdTransaction) EtcdTx(etcdCli *clientv3.Client, lock string) (*clientv3.TxnResponse, *EtcdRollback, error) {

	return t.EtcdTxWithOptions(etcdCli, EtcdTransactionOptions{
		Ctx:             nil,
		TxLimit:         0,
		Lock:            lock,
		IgnoreFailedIfs: false,
		Cache:           nil,
	})
}

// tx_limit will segment the txn into multiple txn's if it's longer than the limit
// note: doing large AND conditionally complicated operations might be sketchy if concurrent etcd writes are being done
func (t *EtcdTransaction) EtcdTxWithOptions(etcdCli *clientv3.Client, options EtcdTransactionOptions) (*clientv3.TxnResponse, *EtcdRollback, error) {

	t.options = &options

	if options.Ctx == nil {
		ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
		defer cancel()
		options.Ctx = ctx
	}

	if len(options.Lock) > 0 {
		l, err := NewLock(options.Ctx, etcdCli, options.Lock)
		if err != nil {
			return nil, nil, err
		}
		defer func() {
			err := l.Unlock()
			if err != nil {
				log.Error(err)
			}
		}()
	}

	if options.TxLimit == 0 {
		options.TxLimit = DEFAULT_TXN_LIMIT
	}

	// make a copy to avoid modifying t.Ifs
	ifs := t.Ifs[:]
	var updates []clientv3.Op

	t.txnToOpsMap = make([]int, len(t.Ops))
	txn_index := 0

	if t.options.StartRevision < 0 {
		rev, err := GetCurrentRevision(etcdCli)
		if err != nil {
			return nil, nil, err
		}

		t.options.StartRevision = rev
	}

	// gather operations into the etcd format
	for i, a := range t.Ops {
		t.Ops[i].returnedResponse = nil
		t.Ops[i].responseFromCache = false

		switch a.Op {

		case Write:
			if ts, ok := a.Object.(EtcdTxTimestamped); ok {
				ts.WriteTimestamp()
			}

			data, err := MarshalEtcdObjectIO(a.Object)
			if err != nil {
				return nil, nil, err
			}

			ops := append(a.Opts, clientv3.WithPrevKV())

			updates = append(updates,
				clientv3.OpPut(a.Object.Key(), string(data), ops...),
			)
			t.txnToOpsMap[txn_index] = i
			txn_index++

			if a.Object.GetVersion() >= 0 {
				ifs = append(ifs,
					clientv3.Compare(
						clientv3.Version(a.Object.Key()),
						"=",
						a.Object.GetVersion(),
					),
				)
			}

		case Delete:
			ops := append(a.Opts, clientv3.WithPrevKV())

			updates = append(updates,
				clientv3.OpDelete(a.Object.Key(), ops...),
			)
			t.txnToOpsMap[txn_index] = i
			txn_index++

			if a.Object.GetVersion() >= 0 {
				ifs = append(ifs,
					clientv3.Compare(
						clientv3.Version(a.Object.Key()),
						"=",
						a.Object.GetVersion(),
					),
				)
			}

		case Read:
			op := clientv3.OpGet(a.Object.Key(), a.Opts...)

			fromCache := func() bool {
				if options.Cache == nil {
					return false
				}

				// range request
				if op.RangeBytes() != nil {
					ges := options.Cache.GetRange(string(op.KeyBytes()), string(op.RangeBytes()), a.Opts...)
					if len(ges) == 0 {
						return false
					}

					t.Ops[i].returnedResponse = keysToTxrResponses(ges...)
					t.Ops[i].responseFromCache = true
					return true
				}

				// regular put request
				ge, found := options.Cache.Get(string(op.KeyBytes()), a.Opts...)
				if !found {
					return false
				}

				t.Ops[i].returnedResponse = keysToTxrResponses(ge)
				t.Ops[i].responseFromCache = true
				return true
			}()

			if !fromCache {
				// apply the revision to the operation
				if t.options.StartRevision > 0 {
					clientv3.WithRev(t.options.StartRevision)(&op)
				}

				updates = append(updates, op)
				t.txnToOpsMap[txn_index] = i
				txn_index++
			}
		}
	}

	// everything is in the cache (so read only), so don't even bother doing an etcd txn
	if options.Cache != nil && len(updates) == 0 {
		log.Tracef("all reads were cached, no etcd connection")

		txr := &clientv3.TxnResponse{
			Header: &pb.ResponseHeader{
				// signifies that we read nothing
				Revision: -1,
			},
			Succeeded: true,
		}

		err := t.updateOpsFromTxr(txr)
		if err != nil {
			log.Warnf("error: t.updateOps: %+v", err)
		}

		// no rollback, since we didn't read anything

		return txr, nil, nil

	}

	kvc := clientv3.NewKV(etcdCli)

	// smaller than limit, so we can do it in one transation
	if options.TxLimit <= 0 || len(updates) < options.TxLimit {

		txr, err := kvc.Txn(options.Ctx).If(ifs...).Then(updates...).Commit()
		if err != nil {
			return nil, nil, err
		}
		if !txr.Succeeded {
			err := fmt.Errorf("transaction failed: ifs preconditions weren't met")
			if options.IgnoreFailedIfs {
				err = nil
			}

			return txr, nil, err
		}

		err = t.updateOpsFromTxr(txr)
		if err != nil {
			log.Warnf("error: t.updateOps: %+v", err)
		}

		// the transaction succeeded, so provide a rollback transaction
		// for callers to use if this update needs to be undone

		rb, err := t.constructRollback(options.Ctx, etcdCli, txr, updates)
		if err != nil {
			log.Warnf("error: t.constructRollback: %+v", err)
		}

		return txr, rb, nil
	}

	// let's try to segment the transation
	length := 0
	var ifs_segment []clientv3.Cmp
	var collected_txr *clientv3.TxnResponse

	push_ifs := func() error {

		txr, err := kvc.Txn(options.Ctx).If(ifs_segment...).Then().Commit()

		// on error, don't need to error because we never wrote anything
		if err != nil {
			return err
		}

		collected_txr = JoinTxr(collected_txr, txr)

		if !collected_txr.Succeeded {
			err := fmt.Errorf("transaction failed: preconditions weren't met")
			if options.IgnoreFailedIfs {
				err = nil
			}
			return err
		}

		ifs_segment = nil
		length = 0

		return nil
	}

	// first, we need to check the ifs
	for _, iff := range ifs {
		length++
		ifs_segment = append(ifs_segment, iff)

		// push the segment
		if length >= options.TxLimit {
			err := push_ifs()
			if err != nil {
				return collected_txr, nil, err
			}
		}
	}

	// push the last segment
	err := push_ifs()
	if err != nil {
		return collected_txr, nil, err
	}

	// now, just push everything
	var update_segments []clientv3.Op

	push_updates := func() error {

		txr, err := kvc.Txn(options.Ctx).Then(update_segments...).Commit()

		// on an error, we should rollback to the best of our ability by ourselves
		if err != nil {
			rb, err := t.constructRollback(options.Ctx, etcdCli, txr, updates)
			if err == nil {
				rb.Rollback()
			} else {
				log.Warnf("error: t.constructRollback: %+v", err)
			}

			return err
		}

		collected_txr = JoinTxr(collected_txr, txr)

		update_segments = nil
		length = 0

		return nil
	}

	for _, u := range updates {
		length++
		update_segments = append(update_segments, u)

		// push the segment
		if length >= options.TxLimit {
			err := push_updates()
			if err != nil {
				return collected_txr, nil, err
			}
		}
	}

	// push the last segment
	err = push_updates()
	if err != nil {
		return collected_txr, nil, err
	}

	// in theory, that should be everything, update ops and provide a rollback
	err = t.updateOpsFromTxr(collected_txr)
	if err != nil {
		log.Warnf("error: t.updateOps: %+v", err)
	}

	rb, err := t.constructRollback(options.Ctx, etcdCli, collected_txr, updates)
	if err != nil {
		log.Warnf("error: t.constructRollback: %+v", err)
	}

	return collected_txr, rb, nil

}

func (t *EtcdTransaction) updateOpsFromTxr(txr *clientv3.TxnResponse) error {
	if txr == nil {
		return nil
	}

	if t.options.Cache != nil {
		t.options.Cache.bumpRevision(txr.Header.GetRevision())

		// assiociate the responses with the op that it belongs to
		for txnIndex, r := range txr.Responses {
			i := t.txnToOpsMap[txnIndex]

			if t.Ops[i].returnedResponse != nil {
				return fmt.Errorf("somehow, ops[%d] already has a returned response", i)
			}

			t.Ops[i].returnedResponse = r
		}

		// join together both cached and returned responses
		txr.Responses = make([]*pb.ResponseOp, len(t.Ops))
		for i, o := range t.Ops {
			txr.Responses[i] = o.returnedResponse
		}
	}

	for i, r := range txr.Responses {
		o := t.Ops[i].Object

		// collect responses to put operations in the update transaction

		rp := r.GetResponsePut()
		if rp != nil {

			kv := rp.PrevKv

			if kv == nil {
				o.SetVersion(1)
			} else {
				o.SetVersion(kv.Version + 1)
			}

			if t.options.Cache != nil && !t.Ops[i].responseFromCache {
				ge, err := NewGenericEtcdKeyFromEtcdObjectIO(o)
				if err != nil {
					log.Errorf("error making etcd key from object io, skipping: %+v", err)
					continue
				}

				t.options.Cache.Set(*ge)
			}
		}

		rr := r.GetResponseRange()
		if rr != nil {

			for _, kv := range rr.Kvs {

				if t.options.Cache != nil && !t.Ops[i].responseFromCache {
					t.options.Cache.Set(*NewGenericEtcdKeyFromKV(kv))
				}

				if o.Key() != string(kv.Key) {
					continue
				}

				err := UnmarshalEtcdObjectIO(o, kv.Value)
				if err != nil {
					return err
				}

				o.SetVersion(kv.Version)

			}
		}

		rd := r.GetResponseDeleteRange()
		if rd != nil {

			for _, kv := range rd.PrevKvs {

				if t.options.Cache != nil {
					t.options.Cache.Delete(string(kv.Key))
				}

				if o.Key() != string(kv.Key) {
					continue
				}

				o.SetVersion(0)
			}
		}
	}

	return nil

}

func (t *EtcdTransaction) constructRollback(ctx context.Context, etcdCli *clientv3.Client, txr *clientv3.TxnResponse, updates []clientv3.Op) (*EtcdRollback, error) {
	if txr == nil {
		return nil, nil
	}

	var rollback []clientv3.Op

	for i, r := range txr.Responses {

		// collect responses to put operations in the update transaction
		rp := r.GetResponsePut()
		if rp != nil {

			kv := rp.PrevKv

			if kv == nil {
				// this means the key was not present before the update, so we
				// need to delete it on rollback
				k := string(updates[i].KeyBytes())
				rollback = append(rollback, clientv3.OpDelete(k))
			} else {
				// the previous revision of the key was provided,
				// use this as the rollback
				rollback = append(rollback,
					clientv3.OpPut(string(kv.Key), string(kv.Value)),
				)
			}

		}

		// collect responses to delete operations in the update transaction
		rd := r.GetResponseDeleteRange()
		if rd != nil {

			for _, kv := range rd.PrevKvs {
				rollback = append(rollback,
					clientv3.OpPut(string(kv.Key), string(kv.Value)),
				)
			}
		}
	}

	kvc := clientv3.NewKV(etcdCli)
	rb := (EtcdRollback)(func() (*clientv3.TxnResponse, error) {

		return kvc.Txn(ctx).Then(rollback...).Commit()
	})

	return &rb, nil
}

func (t *EtcdTransaction) EtcdWatch(etcdCli *clientv3.Client, options EtcdTransactionOptions) (EtcdWatchChannel, error) {
	if len(t.Ifs) > 0 {
		return nil, fmt.Errorf("ifs not supported in watch")
	}

	for _, a := range t.Ops {
		if a.Op != Read {
			return nil, fmt.Errorf("put/delete not supported in watch")
		}
	}

	if options.Ctx == nil {
		return nil, fmt.Errorf("cannot use nil context, use one with cancel to release watch resources when finished")
	}

	if options.StartRevision < 0 {
		rev, err := GetCurrentRevision(etcdCli)
		if err != nil {
			return nil, err
		}

		options.StartRevision = rev
	}

	t.options = &options

	ch := make(chan EtcdWatchResponse)

	var wg sync.WaitGroup
	wg.Add(len(t.Ops))

	for i, a := range t.Ops {
		i := i
		k := a.Object.Key()
		opts := a.Opts[:]
		opts = append(opts,
			clientv3.WithProgressNotify(),
			clientv3.WithFragment(),
		)

		if t.options.StartRevision > 0 {
			opts = append(opts, clientv3.WithRev(t.options.StartRevision))
		}

		go func() {
			defer wg.Done()

			watch_chan := etcdCli.Watch(options.Ctx, k, opts...)
			for {
				select {
				case <-options.Ctx.Done():
					return
				case wr, more := <-watch_chan:
					if !more {
						return
					}

					t.updateOpsFromWatch(i, wr)
					ch <- EtcdWatchResponse{WatchResponse: wr, IndexOfWatch: i}

				}
			}
		}()
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	return ch, nil

}

func (t *EtcdTransaction) updateOpsFromWatch(i int, wr clientv3.WatchResponse) error {
	if t.options.Cache != nil {
		t.options.Cache.bumpRevision(wr.Header.GetRevision())
	}

	for _, e := range wr.Events {
		o := t.Ops[i].Object

		switch e.Type {
		case mvccpb.PUT:
			if t.options.Cache != nil {
				t.options.Cache.Set(*NewGenericEtcdKeyFromKV(e.Kv))
			}

			if o.Key() == string(e.Kv.Key) {
				err := UnmarshalEtcdObjectIO(o, e.Kv.Value)
				if err != nil {
					return err
				}

				o.SetVersion(e.Kv.Version)
			}
		case mvccpb.DELETE:
			if t.options.Cache != nil {
				t.options.Cache.Delete(e.Kv.String())
			}

			if o.Key() == string(e.Kv.Key) {
				err := UnmarshalEtcdObjectIO(o, e.Kv.Value)
				if err != nil {
					return err
				}

				o.SetVersion(0)
			}
		default:
			return fmt.Errorf("unknown MVCCPB event")
		}
	}

	return nil
}
