package etcd_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/mergetb/tech/shared/storage/etcd"
)

func TestCacheGetSet(t *testing.T) {
	a := assert.New(t)

	cycle := 100 * time.Millisecond
	c := etcd.NewReadCache(cycle, func(item etcd.GenericEtcdKey) time.Duration {
		switch item.EtcdKey {
		case "sticky":
			return 0
		default:
			return cycle / 2
		}
	})
	defer c.Close()

	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "sticky",
			EtcdValue: []byte("forever"),
		},
	)

	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "hello",
			EtcdValue: []byte("Hello"),
		},
	)
	hello, found := c.Get("hello")

	a.True(
		found,
		"Should exist",
	)

	a.Equal(
		string(hello.EtcdValue),
		"Hello",
		"Two should be identical",
	)

	time.Sleep(cycle / 2)

	_, found = c.Get("hello")

	a.False(
		found,
		"Should not exist",
	)

	time.Sleep(cycle)

	_, found = c.Get("404")

	a.False(
		found,
		"Should not exist",
	)

	_, found = c.Get("sticky")

	a.True(
		found,
		"Should exist",
	)

}

func TestCacheDelete(t *testing.T) {
	c := etcd.NewReadCache(0, nil)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "hello",
			EtcdValue: []byte("Hello"),
		},
	)
	_, found := c.Get("hello")

	if !found {
		t.FailNow()
	}

	c.Delete("hello")

	_, found = c.Get("hello")

	if found {
		t.FailNow()
	}
}

func TestCacheGetAll(t *testing.T) {
	c := etcd.NewReadCache(0, nil)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "hello",
			EtcdValue: []byte("Hello"),
		},
	)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "world",
			EtcdValue: []byte("World"),
		},
	)

	assert.Equal(
		t,
		2,
		len(c.GetAll()),
		"Should have two elements",
	)
}

func TestCacheGetPrefix(t *testing.T) {
	c := etcd.NewReadCache(0, nil)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "/hello/world",
			EtcdValue: []byte("Hello"),
		},
	)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "/hello/test",
			EtcdValue: []byte("World"),
		},
	)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "/beans/world",
			EtcdValue: []byte("World"),
		},
	)

	assert.Equal(
		t,
		2,
		len(c.GetPrefix("/hello/")),
		"Should have two elements",
	)

	assert.Equal(
		t,
		1,
		len(c.GetPrefix("/beans/")),
		"Should have one elements",
	)
}

func TestCacheGetAllTimer(t *testing.T) {
	c := etcd.NewReadCache(0,
		func(item etcd.GenericEtcdKey) time.Duration {
			return time.Nanosecond
		},
	)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "hello",
			EtcdValue: []byte("Hello"),
		},
	)
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "world",
			EtcdValue: []byte("World"),
		},
	)
	time.Sleep(time.Microsecond)

	assert.Equal(
		t,
		0,
		len(c.GetAll()),
		"Should have no elements",
	)
}

func BenchmarkCacheNew(b *testing.B) {
	b.ReportAllocs()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			etcd.NewReadCache(5*time.Second, nil).Close()
		}
	})
}

func BenchmarkCacheGet(b *testing.B) {
	c := etcd.NewReadCache(5*time.Second, nil)
	defer c.Close()
	c.Set(
		etcd.GenericEtcdKey{
			EtcdKey:   "hello",
			EtcdValue: []byte("Hello"),
		},
	)

	b.ReportAllocs()
	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			c.Get("Hello")
		}
	})
}

func BenchmarkCacheSet(b *testing.B) {
	c := etcd.NewReadCache(5*time.Second, nil)
	defer c.Close()

	b.ResetTimer()
	b.ReportAllocs()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			c.Set(
				etcd.GenericEtcdKey{
					EtcdKey:   "hello",
					EtcdValue: []byte("Hello"),
				},
			)
		}
	})
}

func BenchmarkCacheDelete(b *testing.B) {
	c := etcd.NewReadCache(5*time.Second, nil)
	defer c.Close()

	b.ResetTimer()
	b.ReportAllocs()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			c.Delete("Hello")
		}
	})
}
