package etcd

import (
	"context"
	"fmt"

	"go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
)

var (
	locksRoot string = "/.lock"
)

type Lock struct {
	path    string
	mutex   *concurrency.Mutex
	session *concurrency.Session
}

type Locks []*Lock

func NewLock(ctx context.Context, etcdCli *clientv3.Client, path string) (*Lock, error) {

	path = SmartJoin(locksRoot, path)

	se, err := concurrency.NewSession(etcdCli)
	if err != nil {
		return nil, fmt.Errorf("lock session: %v", err)
	}

	lk := &Lock{
		path:    path,
		session: se,
		mutex:   concurrency.NewMutex(se, path),
	}

	err = lk.mutex.Lock(ctx)
	if err != nil {
		se.Close()
		return nil, fmt.Errorf("lock %s: %v", path, err)
	}

	return lk, nil

}

func NewLocks(ctx context.Context, etcdCli *clientv3.Client, paths ...string) (Locks, error) {

	var lks Locks

	for _, p := range paths {
		lk, err := NewLock(ctx, etcdCli, p)
		if err != nil {
			lks.Unlock()
			return nil, err
		}
		lks = append(lks, lk)
	}

	return lks, nil

}

func NewLockFromObj(ctx context.Context, etcdCli *clientv3.Client, o EtcdObject) (*Lock, error) {
	return NewLock(ctx, etcdCli, o.Key())
}

func NewLockFromObjs(ctx context.Context, etcdCli *clientv3.Client, objects ...EtcdObject) (Locks, error) {

	var paths []string
	for _, o := range objects {
		paths = append(paths, o.Key())
	}

	return NewLocks(ctx, etcdCli, paths...)

}

func (lks Locks) Unlock() []error {

	if lks == nil {
		return nil
	}

	var errs []error

	for _, lk := range lks {

		err := lk.Unlock()
		if err != nil {
			errs = append(errs, err)
		}

	}

	return errs

}

func (lk *Lock) Unlock() error {

	if lk == nil {
		return nil
	}

	defer lk.session.Close()

	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	err := lk.mutex.Unlock(ctx)
	if err != nil {
		return fmt.Errorf("unlock %s: %v", lk.path, err)
	}

	return nil

}
